The examples provided in this directory support the numerical experiments
discussed in the paper:

P. Houston & N. Sime, Automatic Symbolic Computation for Discontinuous
Galerkin Finite Element Methods. Submitted for publication.